\contentsline {section}{\numberline {1}Za\IeC {\l }o\IeC {\.z}enia projektowe}{2}{section.1}
\contentsline {section}{\numberline {2}Symulator systemu}{2}{section.2}
\contentsline {subsection}{\numberline {2.1}Generacja danych przewidziana do transmisji danych}{2}{subsection.2.1}
\contentsline {subsection}{\numberline {2.2}Kodowanie wysy\IeC {\l }anych danych w nadajniku}{2}{subsection.2.2}
\contentsline {subsection}{\numberline {2.3}Zamiana niekt\IeC {\'o}rych danych w wyniku zak\IeC {\l }\IeC {\'o}ce\IeC {\'n} transmisyjnych}{2}{subsection.2.3}
\contentsline {subsection}{\numberline {2.4}Dekodowanie odebranych danych w odbiorniku}{3}{subsection.2.4}
\contentsline {section}{\numberline {3}Przyk\IeC {\l }adowe wyniki eksperymentu dla 10 pr\IeC {\'o}bek.}{4}{section.3}
\contentsline {section}{\numberline {4}Analiza statystyczna.}{6}{section.4}
\contentsline {subsection}{\numberline {4.1}Obliczanie BER dla r\IeC {\'o}\IeC {\.z}nych parametr\IeC {\'o}w systemu.}{6}{subsection.4.1}
\contentsline {subsection}{\numberline {4.2}Wyznaczanie rozk\IeC {\l }adu b\IeC {\l }\IeC {\k e}d\IeC {\'o}w transmisji w czasie.}{7}{subsection.4.2}
\contentsline {subsection}{\numberline {4.3}Wyznaczanie parametr\IeC {\'o}w opisuj\IeC {\k a}cych efektywno\IeC {\'s}\IeC {\'c} transmisji.}{7}{subsection.4.3}
\contentsline {section}{\numberline {5}Optymalizacja systemu.}{7}{section.5}
\contentsline {subsection}{\numberline {5.1}Rozwi\IeC {\k a}zanie zadania.}{7}{subsection.5.1}
\contentsline {section}{\numberline {6}Wnioski i podsumowanie.}{7}{section.6}
\contentsline {section}{\numberline {7}Link do \IeC {\'z}r\IeC {\'o}d\IeC {\l }a program\IeC {\'o}w opracowanych w projekcie.}{8}{section.7}
\contentsline {section}{\numberline {8}Bibliografia}{9}{section.8}
