%generowanie sygnału normalnego
n=1000;
tab = zeros(1,n);
 
    for j=1:n;
        randHelper = normrnd(0,1); %unormowanie (rozkład normalny), 0 początek -> 1 odchylenie standardowe czyli -1 do 1
        if(randHelper < 0)
             tab(1,j)=1; end;
        if (randHelper >= 0)
             tab(1,j)=0; end;
    end;

%generowanie nośnej 

fp=200;
t=0:1/fp:(n+10);        
x=cos(2*pi*t);
 
 for i=1:n
    bit = tab(i);
    time = i-1;
   
   if bit == 0
      x((time*fp)+1 : (fp*i)+1) = x((time*fp)+1 + (fp/2) : (fp*i)+1 + (fp/2)); %moduloacja BSK nośnej
    end
   
 end

%wykres

y=x(1: (fp*n));
figure(1)
subplot(3,1,1);
axis([0 12 -0.5 1]), grid on;
plot( y, 'g',  'LineWidth',2), axis([0 fp*n -2 2]), grid on;
 
xlabel('\bfCzas');
ylabel('\bfAmplituda');
title('\bfModulator BSK');