%generowanie sygna�u normalnego
n=1000;
tab = zeros(1,n);
 
    for j=1:n;
        randHelper = normrnd(0,1); %unormowanie (rozk�ad normalny), 0 pocz�tek -> 1 odchylenie standardowe czyli -1 do 1
        if(randHelper < 0)
             tab(1,j)=1; end;
        if (randHelper >= 0)
             tab(1,j)=0; end;
    end;

%generowanie no�nej 

fp=200;
t=0:1/fp:(n+10);        
x=cos(2*pi*t);
 
 for i=1:n
    bit = tab(i);
    time = i-1;
   
   if bit == 0
      x((time*fp)+1 : (fp*i)+1) = x((time*fp)+1 + (fp/2) : (fp*i)+1 + (fp/2)); %moduloacja BSK no�nej
    end
   
 end

%wykres

y=x(1: (fp*n));
figure(1)
subplot(3,1,1);
axis([0 12 -0.5 1]), grid on;
plot( y, 'g',  'LineWidth',2), axis([0 fp*n -2 2]), grid on;
 
xlabel('\bfCzas');
ylabel('\bfAmplituda');
title('\bfModulator BSK');
 

%KANA� TRANSMISYJNY

 
zakloceniaAmplitudy = zeros(1,n);

for zaklocenia=1:n
        zakloceniaAmplitudy(1,zaklocenia) = normrnd(1, 0.2);   %0.2 prawdopodobie�stwo 
end;

zakloceniaFazy = zeros(1,n);

for zaklocenia=1:n
        zakloceniaFazy(1,zaklocenia) = normrnd(0, 0.2);
        if(zakloceniaFazy(1,zaklocenia)<0)
            zakloceniaFazy(1,zaklocenia)= 1+zakloceniaFazy(1,zaklocenia);
        end
      
end;

%zakloceniaFazy
xx=cos(2*pi*t);

 for i=1:n
    bit = tab(i);
    time = i-1;
   
    zakFaz=round(fp* zakloceniaFazy(1,i));
    
    if bit == 1
       x((time*fp)+1 : (fp*i)+1) = zakloceniaAmplitudy(1,i)*xx((time*fp)+1+zakFaz : (fp*i)+1+zakFaz);
    end
  
   if bit == 0
       x((time*fp)+1 : (fp*i)+1) = zakloceniaAmplitudy(1,i)*xx((time*fp)+1 + (fp/2)+ zakFaz : (fp*i)+1 + (fp/2)+zakFaz);
    end
   
 end
 
y=x(1: (fp*n));



subplot(3,1,2);
plot( y, 'm',  'LineWidth',2), axis([0 fp*n -2 2]), grid on;
 
xlabel('\bfCzas');
ylabel('\bfAmplituda');
title('\bfModulator BSK z zak��ceniami');


%TWORZENIE OSI X i Y DLA KONSTELACJI
osX = zeros(1,n);
osY = zeros(1,n);
figure(2);

%rysowanie konstelacji dla sygna�u z zak�uceniami amplitudy i fazy, nie
%sygna� zak�ucony, tylko warto�ci
for i=1:n
    if tab(i) == 1
        osX(i) = zakloceniaAmplitudy(i)*cos(2*pi+zakloceniaFazy(i)*2*pi);
        osY(i) = zakloceniaAmplitudy(i)*sin(2*pi+zakloceniaFazy(i)*2*pi);
        %plot(osX(i), osY(i), 'ro',  'LineWidth',1), axis([-2 2 -2 2]), grid on; 
    else
        osX(i) = zakloceniaAmplitudy(i)*cos(2*pi+zakloceniaFazy(i)*2*pi+pi);
        osY(i) = zakloceniaAmplitudy(i)*sin(2*pi+zakloceniaFazy(i)*2*pi+pi);
        %plot(osX(i), osY(i), 'bo',  'LineWidth',1), axis([-2 2 -2 2]), grid on;
    end
end

%--------------------------------------------------------------------------

%RYSOWANIE KONSTELACJI OSI I KOLOROWANIE
figure(2)
subplot(1,1,1);
hold on;
xL = [-2 2];
yL = [-2 2];
line([0 0], yL);  %x-axis, czyli okre�lanie/skalowanie osi
line(xL, [0 0]);  %y-axis
title('\bfKonstelacja Modulatora BSK z zak��ceniami');

for i=1:n
    if tab(i) == 1
        plot(osX(i), osY(i), 'ro',  'LineWidth',1), axis([-2 2 -2 2]), grid on;
    else
        plot(osX(i), osY(i), 'go',  'LineWidth',1), axis([-2 2 -2 2]), grid on;
    end
end

hold off;