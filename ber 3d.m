%generowanie sygna�u normalnego
n=1000;
tab = zeros(1,n);
 
    for j=1:n;
        randHelper = normrnd(0,1); %unormowanie (rozk�ad normalny), 0 pocz�tek -> 1 odchylenie standardowe czyli -1 do 1
        if(randHelper < 0)
             tab(1,j)=1; end;
        if (randHelper >= 0)
             tab(1,j)=0; end;
    end;

%generowanie no�nej 

fp=200;
t=0:1/fp:(n+10);        
x=cos(2*pi*t);
 
 for i=1:n
    bit = tab(i);
    time = i-1;
   
   if bit == 0
      x((time*fp)+1 : (fp*i)+1) = x((time*fp)+1 + (fp/2) : (fp*i)+1 + (fp/2)); %moduloacja BSK no�nej
    end
   
 end

%wykres

y=x(1: (fp*n));
figure(1)
subplot(3,1,1);
axis([0 12 -0.5 1]), grid on;
plot( y, 'g',  'LineWidth',2), axis([0 fp*n -2 2]), grid on;
 
xlabel('\bfCzas');
ylabel('\bfAmplituda');
title('\bfModulator BSK');
 

%KANA� TRANSMISYJNY

 
zakloceniaAmplitudy = zeros(1,n);

for zaklocenia=1:n
        zakloceniaAmplitudy(1,zaklocenia) = normrnd(1, 0.2);   %0.2 prawdopodobie�stwo 
end;

zakloceniaFazy = zeros(1,n);

for zaklocenia=1:n
        zakloceniaFazy(1,zaklocenia) = normrnd(0, 0.2);
        if(zakloceniaFazy(1,zaklocenia)<0)
            zakloceniaFazy(1,zaklocenia)= 1+zakloceniaFazy(1,zaklocenia);
        end
      
end;

%zakloceniaFazy
xx=cos(2*pi*t);

 for i=1:n
    bit = tab(i);
    time = i-1;
   
    zakFaz=round(fp* zakloceniaFazy(1,i));
    
    if bit == 1
       x((time*fp)+1 : (fp*i)+1) = zakloceniaAmplitudy(1,i)*xx((time*fp)+1+zakFaz : (fp*i)+1+zakFaz);
    end
  
   if bit == 0
       x((time*fp)+1 : (fp*i)+1) = zakloceniaAmplitudy(1,i)*xx((time*fp)+1 + (fp/2)+ zakFaz : (fp*i)+1 + (fp/2)+zakFaz);
    end
   
 end
 
y=x(1: (fp*n));



subplot(3,1,2);
plot( y, 'm',  'LineWidth',2), axis([0 fp*n -2 2]), grid on;
 
xlabel('\bfCzas');
ylabel('\bfAmplituda');
title('\bfModulator BSK z zak��ceniami');


%TWORZENIE OSI X i Y DLA KONSTELACJI
osX = zeros(1,n);
osY = zeros(1,n);
figure(2);

%rysowanie konstelacji dla sygna�u z zak�uceniami amplitudy i fazy, nie
%sygna� zak�ucony, tylko warto�ci
for i=1:n
    if tab(i) == 1
        osX(i) = zakloceniaAmplitudy(i)*cos(2*pi+zakloceniaFazy(i)*2*pi);
        osY(i) = zakloceniaAmplitudy(i)*sin(2*pi+zakloceniaFazy(i)*2*pi);
        %plot(osX(i), osY(i), 'ro',  'LineWidth',1), axis([-2 2 -2 2]), grid on; 
    else
        osX(i) = zakloceniaAmplitudy(i)*cos(2*pi+zakloceniaFazy(i)*2*pi+pi);
        osY(i) = zakloceniaAmplitudy(i)*sin(2*pi+zakloceniaFazy(i)*2*pi+pi);
        %plot(osX(i), osY(i), 'bo',  'LineWidth',1), axis([-2 2 -2 2]), grid on;
    end
end

%--------------------------------------------------------------------------




%RYSOWANIE KONSTELACJI OSI I KOLOROWANIE
figure(2)
subplot(1,1,1);
hold on;
xL = [-2 2];
yL = [-2 2];
line([0 0], yL);  %x-axis, czyli okre�lanie/skalowanie osi
line(xL, [0 0]);  %y-axis
title('\bfKonstelacja Modulatora BSK z zak��ceniami');

for i=1:n
    if tab(i) == 1
        plot(osX(i), osY(i), 'ro',  'LineWidth',1), axis([-2 2 -2 2]), grid on;
    else
        plot(osX(i), osY(i), 'go',  'LineWidth',1), axis([-2 2 -2 2]), grid on;
    end
end

hold off;

%TWORZENIE TABLICY PO DEMODULACJI

for i=1:n
    if osX(i) >= 0
        tab2(i)=1;
    else
        tab2(i)=0;
    end
end

%--------------------------------------------------------------------------

fp=200;
t=0:1/fp:(n+10);
x=cos(2*pi*t);
 
 for i=1:n
    bit = tab2(i);
    time = i-1;
   
   if bit == 0
      x((time*fp)+1 : (fp*i)+1) = x((time*fp)+1 + (fp/2) : (fp*i)+1 + (fp/2));
    end
   
 end
 
y=x(1: (fp*n));

figure(1)
subplot(3,1,3);
axis([0 12 -0.5 1]), grid on;
plot( y, 'b',  'LineWidth',2), axis([0 fp*n -2 2]), grid on;
 
xlabel('\bfCzas');
ylabel('\bfAmplituda');
title('\bfDemodulator BSK');

%---------------------------------------------------------------------
%rysowanie wykresu 3D

j=0;
for i=1:10 % p�tla zewn�trzna
    
    k=0;
    for m=1:10 % p�tla wewn�trzna
        
        zaklAmplitudy = zeros(1,n);
        for zaklocenia=1:n
            zaklAmplitudy(1,zaklocenia) = normrnd(1, k);    
        end

        zaklFazy = zeros(1,n);
        for zaklocenia=1:n
            zaklFazy(1,zaklocenia) = normrnd(0, j);
            if(zaklFazy(1,zaklocenia)<0)
                zaklFazy(1,zaklocenia)= 1+zaklFazy(1,zaklocenia);
            end    
        end
        
        osX_1 = zeros(1,n);
        
        for a=1:n
            if tab(a) == 1
                osX_1(a) = zaklAmplitudy(a)*cos(2*pi+zaklFazy(a)*2*pi);
            else
                osX_1(a) = zaklAmplitudy(a)*cos(2*pi+zaklFazy(a)*2*pi+pi);
            end
        end
        
        tab3 = zeros(1,n);
        
        for b=1:n
            if osX_1(b) >= 0
                tab3(b)=1;
            else
                tab3(b)=0;
            end
        end 
        
        BER = 0;
        
        for c=1:n   
            if tab(c) ~= tab3(c)
                BER = BER + 1;
            end 
        end

        BER = BER / n;
        k = k+0.1;
        metrixBER(i,m) = BER;
    end
    j = j+0.1;
end

figure(3)
%surf(metrixBER), axis([0 10 0 10 0 0.7]);

x = 0.1:0.1:1;
y = 0.1:0.1:1;
[X,Y] = meshgrid(x,y);

surf(X,Y, metrixBER);

title('Wykres BER');
xlabel('\bfOdchylenie st. z zak��cniami amplitudy');
ylabel('\bfOdchylenie st. z zak��cniami fazy');
zlabel('\bfBER');
