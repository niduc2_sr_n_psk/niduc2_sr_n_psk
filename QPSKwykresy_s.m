n=100;
tab = zeros(1,n);

 
    for j=1:n;
        r = normrnd(0,1);
        if(r < 0)
             tab(1,j)=1; end;
        if (r >= 0)
             tab(1,j)=0;end;
    end
   
    for j=1:n;
        tab(1,j);
    end;
 
to_vectors=2*tab-1; %zmiana ulozenia danych
reshapedTab=reshape(to_vectors,2,length(tab)/2);
 
%Czas trwania jednego bitu [s]
fp = 100;
T=1/n;
t=T/fp:T/fp:T;
 
y=[];
 for i=1:n/2
    y1=reshapedTab(1,i)*cos(2*pi*n*t); %skladowa fazowa
    y2=reshapedTab(2,i)*sin(2*pi*n*t); %skladowa kwadratowa
    y=[y y1+y2]; %zmodulowany sygnal
 end
 
subplot(3,1,1);
plot( y, 'r',  'LineWidth',1.5), grid on;

xlabel('\bfCzas');
ylabel('\bfAmplituda');
title('\bfModulator QPSK');


%=========================================
%kana� transmisyjny-------------------------------------------------
%=========================================
zak_amp = zeros(1,n);

for zaklocenia=1:n
        zak_amp(1,zaklocenia) = normrnd(0, 0.2);
        
end;

reshapedTab2 = reshape(zak_amp,2,n/2);
reshapedTab2 = reshapedTab2 + reshapedTab;

T=1/n;
t=T/fp:T/fp:T;

zak_faz = zeros(1,n);

for i=1:n
        zak_faz(1,i) = normrnd(0, 0.1);
        if(zak_faz(1,i)<0)
            zak_faz(1,i)= 1+zak_faz(1,i);
        end
        
        zak_faz(1,i)=round(zak_faz(1,i)*fp);
end;

yy=[];
 for i=1:n/2
     x=zak_faz(1,i);
    y1=reshapedTab2(1,i)*cos(2*pi*n*(t+x)); %skladowa fazowa
    y2=reshapedTab2(2,i)*sin(2*pi*n*(t+x)); %skladowa kwadratowa
    yy=[yy y1+y2]; %zmodulowany sygnal
 end
 

subplot(3,1,2);
plot( yy, 'g',  'LineWidth',1.5), grid on;

xlabel('\bfCzas');
ylabel('\bfAmplituda');
title('\bfModulator QPSK - kana� transmisyjny');



%---------------------------------------------------------------------
osX = zeros(1,n);
osY = zeros(1,n);
zak_faz1 = zak_faz/fp;
zak_amp1 = zak_amp + 1;

figure(2)
subplot(1,1,1);
hold on;
xL = [-2 2];
yL = [-2 2];
line([0 0], yL);  %x-axis
line(xL, [0 0]);  %y-axis
title('\bfKonstelacja Modulatora QPSK z zak��ceniami');

for i=1:2:n
    if tab(i) == 1 && tab(i+1) == 1
        osX(i) = zak_amp1(i)*cos(2*pi+zak_faz1(i)*2*pi+pi/4);
        osY(i) = zak_amp1(i)*sin(2*pi+zak_faz1(i)*2*pi+pi/4);
        plot(osX(i), osY(i), 'ro',  'LineWidth',1), axis([-2 2 -2 2]), grid on;
    end
    if tab(i) == 0 && tab(i+1) == 1
        osX(i) = zak_amp1(i)*cos(2*pi+zak_faz1(i)*2*pi+3*pi/4);
        osY(i) = zak_amp1(i)*sin(2*pi+zak_faz1(i)*2*pi+3*pi/4);
        plot(osX(i), osY(i), 'go',  'LineWidth',1), axis([-2 2 -2 2]), grid on;
    end
    if tab(i) == 0 && tab(i+1) == 0
        osX(i) = zak_amp1(i)*cos(2*pi+zak_faz1(i)*2*pi+5*pi/4);
        osY(i) = zak_amp1(i)*sin(2*pi+zak_faz1(i)*2*pi+5*pi/4);
        plot(osX(i), osY(i), 'mo',  'LineWidth',1), axis([-2 2 -2 2]), grid on;
    end
    if tab(i) == 1 && tab(i+1) == 0
        osX(i) = zak_amp1(i)*cos(2*pi+zak_faz1(i)*2*pi+7*pi/4);
        osY(i) = zak_amp1(i)*sin(2*pi+zak_faz1(i)*2*pi+7*pi/4);
        plot(osX(i), osY(i), 'bo',  'LineWidth',1), axis([-2 2 -2 2]), grid on;
    end
end
hold off;
y = [osX osY];

%---------------------------------------------------------------------
%demodulator
tab2 = zeros(1,n);

j = 1;
for i=1:n
    if osX(i) == 0 && osY(i) == 0   %ciagle rysuje na srodku 1 punkt ktorego nie ma wiec go wykluczam
        continue 
    end
    if osX(i) >= 0 && osY(i) >= 0
        %plot(osX(i), osY(i), 'ro',  'LineWidth',1), axis([-2 2 -2 2]), grid on;
        tab2(j) = 1;
        tab2(j+1) = 1;
        j = j + 2;
    end
    if osX(i) < 0 && osY(i) >= 0
        %plot(osX(i), osY(i), 'go',  'LineWidth',1), axis([-2 2 -2 2]), grid on;
        tab2(j) = 0;
        tab2(j+1) = 1;
        j = j + 2;
    end
    if osX(i) < 0 && osY(i) < 0
        %plot(osX(i), osY(i), 'mo',  'LineWidth',1), axis([-2 2 -2 2]), grid on;
        tab2(j) = 0;
        tab2(j+1) = 0;
        j = j + 2;
    end
    if osX(i) >= 0 && osY(i) < 0
        %plot(osX(i), osY(i), 'bo',  'LineWidth',1), axis([-2 2 -2 2]), grid on;
        tab2(j) = 1;
        tab2(j+1) = 0;
        j = j + 2;
    end
end


to_vectors=2*tab2-1; %zmiana ulozenia danych
reshapedTab=reshape(to_vectors,2,length(tab2)/2);

%Czas trwania jednego bitu [s]
fp = 100;
T=1/n;
t=T/fp:T/fp:T;

y = [];
 for i=1:n/2
    y1=reshapedTab(1,i)*cos(2*pi*n*t); %skladowa fazowa
    y2=reshapedTab(2,i)*sin(2*pi*n*t); %skladowa kwadratowa
    y=[y y1+y2]; %zmodulowany sygnal
 end

figure(1)
subplot(3,1,3);
plot( y, 'm',  'LineWidth',1.5), grid on;

xlabel('\bfCzas');
ylabel('\bfAmplituda');
title('\bfDemdulator QPSK');

%---------------------------------------------------------------------

%-----------------------------------------------------------
% BER

fpp = 100;
j=0;
for i=1:10 % p�tla zewn�trzna
    
    k=0;
    for m=1:10 % p�tla wewn�trzna
        
        zakl_amplitudy = zeros(1,n);
        for zaklocenia=1:n
            zakl_amplitudy(1,zaklocenia) = normrnd(1, k);    
        end

        zakl_fazy = zeros(1,n);
        for ii=1:n
            zakl_fazy(1,ii) = normrnd(0, j);
            
        end
        
        osX_1 = zeros(1,n);
        osY_1 = zeros(1,n);
        
        zakl_fazy1 = zakl_fazy;%/fpp;
        zakl_amp1itudy1 = zakl_amplitudy;% + 1;
        
        for a=1:2:n
            if tab(a) == 1 && tab(a+1) == 1
                osX_1(a) = zakl_amp1itudy1(a)*cos(2*pi+zakl_fazy1(a)*2*pi+pi/4);
                osY_1(a) = zakl_amp1itudy1(a)*sin(2*pi+zakl_fazy1(a)*2*pi+pi/4);
            end
            if tab(a) == 0 && tab(a+1) == 1
                osX_1(a) = zakl_amp1itudy1(a)*cos(2*pi+zakl_fazy1(a)*2*pi+3*pi/4);
                osY_1(a) = zakl_amp1itudy1(a)*sin(2*pi+zakl_fazy1(a)*2*pi+3*pi/4);
            end
            if tab(a) == 0 && tab(a+1) == 0
                osX_1(a) = zakl_amp1itudy1(a)*cos(2*pi+zakl_fazy1(a)*2*pi+5*pi/4);
                osY_1(a) = zakl_amp1itudy1(a)*sin(2*pi+zakl_fazy1(a)*2*pi+5*pi/4);
            end
            if tab(a) == 1 && tab(a+1) == 0
                osX_1(a) = zakl_amp1itudy1(a)*cos(2*pi+zakl_fazy1(a)*2*pi+7*pi/4);
                osY_1(a) = zakl_amp1itudy1(a)*sin(2*pi+zakl_fazy1(a)*2*pi+7*pi/4);
            end
        end
        
        tab4 = zeros(1,n);
        
        o = 1;
for b=1:n
    if osX_1(b) == 0 && osY_1(b) == 0   %ciagle rysuje na srodku 1 punkt ktorego nie ma wiec go wykluczam
        continue 
    end
    if osX_1(b) >= 0 && osY_1(b) >= 0
        %plot(osX(i), osY(i), 'ro',  'LineWidth',1), axis([-2 2 -2 2]), grid on;
        tab4(o) = 1;
        tab4(o+1) = 1;
        o = o + 2;
    end
    if osX_1(b) < 0 && osY_1(b) >= 0
        %plot(osX(i), osY(i), 'go',  'LineWidth',1), axis([-2 2 -2 2]), grid on;
        tab4(o) = 0;
        tab4(o+1) = 1;
        o = o + 2;
    end
    if osX_1(b) < 0 && osY_1(b) < 0
        %plot(osX(i), osY(i), 'mo',  'LineWidth',1), axis([-2 2 -2 2]), grid on;
        tab4(o) = 0;
        tab4(o+1) = 0;
        o = o + 2;
    end
    if osX_1(b) >= 0 && osY_1(b) < 0
        %plot(osX(i), osY(i), 'bo',  'LineWidth',1), axis([-2 2 -2 2]), grid on;
        tab4(o) = 1;
        tab4(o+1) = 0;
        o = o + 2;
    end
end
        BER = 0;
        
        for c=1:n   
            if tab(c) ~= tab4(c)
                BER = BER + 1;
            end 
        end

        BER = BER / n;
        k = k+0.1;
        macierzBER(i,m) = BER;
    end
    j = j+0.1;
end

figure(3)
%surf(macierzBER), axis([0 10 0 10 0 0.7]);

x = 0.1:0.1:1;
y = 0.1:0.1:1;
[X,Y] = meshgrid(x,y);

surf(X,Y, macierzBER);
title('Wykres BER');
xlabel('\bfOdchylenie st. zak��ce� amplitudy');
ylabel('\bfOdchylenie st. zak��ce� fazy');
zlabel('\bfBER');













